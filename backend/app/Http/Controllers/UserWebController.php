<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserWebRequest;
use App\Http\Requests\UpdateUserWebRequest;
use App\Models\UserWeb;

class UserWebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserWebRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserWebRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserWeb  $userWeb
     * @return \Illuminate\Http\Response
     */
    public function show(UserWeb $userWeb)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserWeb  $userWeb
     * @return \Illuminate\Http\Response
     */
    public function edit(UserWeb $userWeb)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserWebRequest  $request
     * @param  \App\Models\UserWeb  $userWeb
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserWebRequest $request, UserWeb $userWeb)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserWeb  $userWeb
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserWeb $userWeb)
    {
        //
    }
}
